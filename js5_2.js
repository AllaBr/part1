class Person {

    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}
const charmed = [new Person ('Piper', 22), new Person('Phoebe', 18), new Person('Prue', 25)];


function mySortAsc(a, b){
    if(a.age < b.age)
        return -1
    if(a.age > b.age)
        return 1
    return 0
}
function mySortDesc(a, b){
    if(a.age < b.age)
        return 1
    if(a.age > b.age)
        return -1
    return 0
}

console.log('Ascending')
charmed.sort(mySortAsc)
console.log(charmed)
console.log('==============')
console.log('Descending')
charmed.sort(mySortDesc)
console.log(charmed)