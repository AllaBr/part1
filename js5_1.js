class Person {

    constructor(name, age) {
        this.name = name;
        this.age = age;
    }
}
function compareAge(p1,p2)
{
    if (p1.age === p2.age)
        return p1.name + 'the same age as ' + p2.name;
    return p1.age > p2.age ? p1.name + ' is older than ' + p2.name : p1.name + ' is younger than ' + p2.name;
}

const Beavis = new Person('Beavis', 25);
const Butthead = new Person('Butthead', 24);

console.log(compareAge(Beavis, Butthead))