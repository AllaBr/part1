function isEven(x)
{
    return x % 2 === 0
}

function doubleFactorial(x)
{
    if (x === 0) {return 1}
    if ('number'!==typeof x) {return 'Input a number please (¬_¬ )'}
    let result = 1
    for (let i = 1; i <= x; i++)
    {
        if (isEven(i) === isEven(x))
        {
            result *= i
        }
    }
    return result
}
console.log(doubleFactorial(9))